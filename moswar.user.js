// ==UserScript==
// @name        Moswar Dungeon
// @namespace   rgnevashev
// @description Moswar Dungeon
// @include     http://www.moswar.ru/*
// @version     1.0.0
// @grant       none
// @require     https://raw.githubusercontent.com/zhil/jquery.timers/master/jquery.timers.js
// ==/UserScript==

var room5MinHeal = 70; // 3
var room10MinHeal = 70; // Метрожка
var room11MinHeal = 70; // 3
var room13MinHeal = 70; // 3
var room21MinHeal = 70; // 4
var room22MinHeal = 90; // Бургер
var openingBox = false;


$(document).ready(function(){
  $('#content').everyTime(1000, function(i) {
    var pageName = $(this).attr('class').trim();
    onTick.bind(this)(pageName);
  })
})

function onTick(state) {
  //console.log(state)
  if (state === 'dungeon dungeon-map' || state === 'dungeon') {
    dungeonAction()
  } else if (state === 'fight-group') {
    fightGroupAction()
  } else if (state === 'fight') {
    fightAction()
  }
}


//------ Actions -----//
function dungeonAction() {
  var roomCurrent = DungeonViewer.roomCurrent
  if ($('#timeout-wrapper').size() ===1 ) {
    console.log('Время кончилось')
    return Groups.leave('dungeon')
  }
  switch (roomCurrent) {
    case 'room-1':
      DungeonViewer.tryToGoToRoom('room-2')
      break;
    case 'room-2':
      if (!dungeonTookBox1()) {
        DungeonViewer.tryToGoToRoom('room-3')
      } else if (!dungeonTookBox3()) {
        if (dungeonMeLife() <= room13MinHeal) {
          Dungeon.heal()
        } else {
          DungeonViewer.tryToGoToRoom('room-13')
        }
      } else {
        Groups.leave('dungeon')
      }
      break;
    case 'room-3':
      if (!dungeonTookBox1()) {
        DungeonViewer.tryToGoToRoom('room-4')
      } else {
        DungeonViewer.tryToGoToRoom('room-2')
      }
      break;
    case 'room-4':
      if (!dungeonTookBox1()) {
        DungeonViewer.tryToGoToRoom('room-5')
      } else {
        DungeonViewer.tryToGoToRoom('room-3')
      }
      break;
    case 'room-5':
      if (dungeonMeLife() <= room5MinHeal) {
        Dungeon.heal()
      } else if (!dungeonTookBox1()) {
        DungeonViewer.tryToGoToRoom('room-6')
      } else {
        DungeonViewer.tryToGoToRoom('room-4')
      }
      break;
    case 'room-6':
      if (!dungeonTookBox1()) {
        DungeonViewer.tryToGoToRoom('room-7')
      } else {
        DungeonViewer.tryToGoToRoom('room-5')
      }
      break;
    case 'room-7':
      if (!dungeonTookBox1()) {
        DungeonViewer.tryToGoToRoom('room-8')
      } else {
        DungeonViewer.tryToGoToRoom('room-6')
      }
      break;
    case 'room-8':
      if (!dungeonPassedMetrozka()) {
        DungeonViewer.tryToGoToRoom('room-9')
      } else if (!dungeonTookBox1()) {
        if (dungeonMeLife() <= room11MinHeal) {
          Dungeon.heal()
        } else {
          DungeonViewer.tryToGoToRoom('room-11')
        }
      } else {
        DungeonViewer.tryToGoToRoom('room-7')
      }
      break;
    case 'room-9':
      if (!dungeonPassedMetrozka()) {
        DungeonViewer.tryToGoToRoom('room-10')
      } else {
        DungeonViewer.tryToGoToRoom('room-8')
      }
      break;
    case 'room-10':
      if (dungeonMeLife() <= room10MinHeal) {
        Dungeon.heal()
      } else if (dungeonCanAttackMetrozka()) {
        Dungeon.useObject(10, 0)
      } else {
        DungeonViewer.tryToGoToRoom('room-9')
      }
      break;
    case 'room-11':
      if (!dungeonTookBox1()) {
        DungeonViewer.tryToGoToRoom('room-12')
      } else {
        DungeonViewer.tryToGoToRoom('room-8')
      }
      break;      
    case 'room-12':
      if (!openingBox) {
       Dungeon.useObject(12, 0)
       openingBox = true
       setTimeout(function(){
         openingBox = false
         DungeonViewer.tryToGoToRoom('room-11')
       }, 3000)
      }      
      break;
    case 'room-13':
      if (!dungeonTookBox3()) {
        DungeonViewer.tryToGoToRoom('room-14')
      } else {
        DungeonViewer.tryToGoToRoom('room-2')
      }      
      break;  
    case 'room-14':
      if (!dungeonPassedBurger()) {
        DungeonViewer.tryToGoToRoom('room-15')
      } else if (!dungeonTookBox3()) {
        DungeonViewer.tryToGoToRoom('room-16')
      } else {
        DungeonViewer.tryToGoToRoom('room-13')
      }      
      break;
    case 'room-15':
      if (!dungeonPassedBurger()) {
        DungeonViewer.tryToGoToRoom('room-17')
      } else {
        DungeonViewer.tryToGoToRoom('room-14')
      }      
      break;      
    case 'room-17':
      if (dungeonMeLife() <= room21MinHeal) {
        Dungeon.heal()
      } else if (!dungeonPassedBurger()) {
        DungeonViewer.tryToGoToRoom('room-21')
      } else {
        DungeonViewer.tryToGoToRoom('room-15')
      }      
      break;     
    case 'room-21':
      if (!dungeonPassedBurger()) {
        DungeonViewer.tryToGoToRoom('room-22')
      } else {
        DungeonViewer.tryToGoToRoom('room-17')
      }      
      break;
    case 'room-22':
      if (dungeonMeLife() <= room22MinHeal) {
        Dungeon.heal()
      } else if (dungeonCanAttackBurger()) {
        Dungeon.useObject(22, 0)
      } else {
        DungeonViewer.tryToGoToRoom('room-21')
      }      
      break;
    case 'room-16':
      if (!dungeonTookBox3()) {
        DungeonViewer.tryToGoToRoom('room-23')
      } else {
        DungeonViewer.tryToGoToRoom('room-14')
      }      
      break;
    case 'room-23':
      if (!dungeonTookBox3()) {
        DungeonViewer.tryToGoToRoom('room-18')
      } else {
        DungeonViewer.tryToGoToRoom('room-16')
      }      
      break;  
    case 'room-18':
      if (!openingBox) {
       Dungeon.useObject(18, 0)
       openingBox = true
       setTimeout(function(){
         openingBox = false
         DungeonViewer.tryToGoToRoom('room-23')
       }, 3000)
      }
      break;
    default:
      console.error('Неизвестная комната')
      break;
  }
}

function fightGroupAction() {
  if (fightGroupIsResult()) {
    redirect('dungeon/inside/')
  } else {
    if (fightGroupIsActions()) {
      if (fightGroupIsBurger()) {
        fightGroupBurger()
      } else if (fightGroupIsMetroza()) {
        fightGroupMetroza()
      } else {
        fightGroup()
      }
    } else {
      //console.log('Ждем завершения хода')
    }
  }
}

function fightAction() {
  redirect('dungeon/inside/')
}

//------ Utils Dungeon -------//
function dungeonTookBox1() {
  return DungeonViewer.rooms['room-12'].passed === 1
}
function dungeonTookBox3() {
  return DungeonViewer.rooms['room-18'].passed === 1
}
function dungeonPassedMetrozka() {
  return DungeonViewer.rooms['room-10'].passed === 1
}
function dungeonCanAttackMetrozka() {
  return DungeonViewer.rooms['room-10'].objects
}
function dungeonPassedBurger() {
  return DungeonViewer.rooms['room-22'].passed === 1
}
function dungeonCanAttackBurger() {
  return DungeonViewer.rooms['room-22'].objects
}
function dungeonMeLife() {
  return Number($('.dungeon-teammate-line.my-line').find('.life .percent').get(0).style.width.replace('%',''))
}


//------ Utils Fight Group -------//
function fightGroup() {
  var step = fightGroupCurrentStep()
  if (step % 2 === 0) {
    $('input[type=radio][rel*="Граната «Дух»"]').click()
  } else {
    $('input[type=radio][rel*="Кластерная граната"]').click()
  }
  groupFightMakeStep()
}
function fightGroupBurger() {
  var step = fightGroupCurrentStep()
  if (step === 0) {
    $('input[type=radio][rel="Призвать мишку"]').click()
  } else if (step === 1) {
    $('input[type=radio][rel*="сыр"]').click()
  } else if (step === 2) {
    $('input[type=radio][rel*="Кальян «Гранатовый»"]').click()
  } else if (step === 3 || step === 4 || step === 5) {
    $('input[type=radio][rel*="Граната «Кислотная»"]').click()
  } else if (step % 4 === 0 && fightGroupBurgerLife() >= 50) {
    $('input[type=radio][rel*="Граната «Дух»"]').click()
  } else {
    $('.list-users.list-users--right').find('li:contains(Бургер мэн) .radio-attack').click()
  }
  groupFightMakeStep()
}
function fightGroupMetroza() {
  var step = fightGroupCurrentStep()
  if (step === 0) {
    $('input[type=radio][rel*="Кальян «Гранатовый»"]').click()
  } else if (step === 1 || step === 2) {
    $('input[type=radio][rel*="Граната «Кислотная»"]').click()
  } else if (step === 3) {
    $('input[type=radio][rel*="Граната «Дух»"]').click()
  } else if (step % 4 === 0) {
    $('input[type=radio][rel*="Граната «Дух»"]').click()
  } else if (step === 5) {
    $('input[type=radio][rel*="сыр"]').click()
  } else {
    $('.list-users.list-users--right').find('li:contains(Метрожа) .radio-attack').click()
  }
  groupFightMakeStep()
}
function fightGroupIsResult() {
  return $('.fight-log').find('.result').size() === 1
}
function fightGroupIsActions() {
  return $('#fightAction').find('.button').is(':visible')
}
function fightGroupCurrentStep() {
  var step = $('.pagescroll .current').text()
  if (step === 'Начало') {
    return 0
  } else {
    return Number(step)
  }
}
function fightGroupMeLife() {
  var me = $('.list-users.list-users--left').find('.me')
  if (me.size()) {
   return Number(me.find('.life .percent').get(0).style.width.replace('%',''))
  } else {
    return false
  }
}
function fightGroupIsBurger() {
  return $('.list-users.list-users--right').find('li:contains(Бургер мэн)').size() === 1
}
function fightGroupBurgerLife() {
  if (fightGroupIsBurger()) {
   return Number($('.list-users.list-users--right').find('li:contains(Бургер мэн)').find('.life .percent').get(0).style.width.replace('%',''))
  } else {
    return false
  }
}
function fightGroupIsMetroza() {
  return $('.list-users.list-users--right').find('li:contains(Метрожа)').size() === 1
}
function fightGroupMetrozaLife() {
  if (fightGroupIsMetroza()) {
   return Number($('.list-users.list-users--right').find('li:contains(Метрожа)').find('.life .percent').get(0).style.width.replace('%',''))
  } else {
    return false
  }
}

//------ Utils Fight -------//

//------ Utils -------//
function redirect(to) {
  location.href = 'http://www.moswar.ru/' + to
}
